#####################################################
# For each nation, produce plots of average discrepancy between GHD and HPI YonY growth rates (quarterly frequency)
# against demographic variables at regional level
# demographic variables are: population, size, density (also in log scale)

# Step 1. Import GHD (including demographics) and HPI data
# Step 2. Compute Laspeyres index at regional level
# Step 3. Join GHD, demographics and HPI and draw plots

# GHD prices and demographic data are imported from \\main.oecd.org\ASgenECO\HOUSING\DATA\National transaction data\_DATA
# HPI data are imported from S:\Data\Prices\Housing\20.Horizontal_project_ECO_CFE_SDD\20.3.Data\HPI\all_countries_national_regional_analysis\A.Original_Data\data_all_update.Rdata

# date: 05 July 2022
#####################################################


# load packages ------------------------------------------------------------------------
packages <- c("gridExtra","RPostgres","sf","stringi","tidyverse")
invisible(lapply(packages,function(x) {library(x,quietly = T, character.only = TRUE)}))
lapply(c('ggplot2','data.table'),packageVersion)

# Main paths ---------------------------------------------------------------------------
main_path <- file.path("S:","Data","Prices","Housing","20.Horizontal_project_ECO_CFE_SDD","20.3.Data","HPI","all_countries_national_regional_analysis")
path_ghd_data <- r"(\\main.oecd.org\ASgenECO\HOUSING\DATA\National transaction data\_DATA)"
path_hpi_data <- file.path(main_path,"A.Original_Data","data_all_update.Rdata")
path_output <- file.path(main_path,"B.Output")
path_code <- file.path(main_path,"C.Programming","laspeyres_index_national_regional.R")


# geographical level at which hpis are available --------------------------------------
country_hpi_level <- data.frame(country=c("USA","GBR","ESP","DEU","FIN","IRL","DNK","PRT","NOR","FRA","AUT","EST","HUN","SWE","BEL","KOR","MEX","ISR"),
                                hpi_level=c("tl2","tl2","tl2","tl1","tl3","tl3","tl3","tl1","tl3","tl3","tl1","tl1","tl2","tl1","tl1","tl1","tl2","tl2"),
                                price_best=c("price_av","price_av","price_av_m2","price_av_m2","price_av_m2","price_av","price_av_m2","price_av_m2","price_m2_av","price_m2_md","price_md_m2","price_av_m2","price_av_m2","price_av_m2","price_av_m2","price_av_m2","price_av_m2","price_av_m2")) %>%
  arrange(country)

countrylist <- country_hpi_level %>% select(country) %>% mutate(country=tolower(country)) %>% pull(country)


# summary tables tl1/tl2/tl3 -----------------------------------------------------------
# function to extract tl3 tl2 codes for each country from gis database
tl_query <- function(select, from,countrycode) {
  paste(
    # Geographies will be projected to EPSG 4326, standard practice to store in oecd_tl
    "SELECT ", select,
    "FROM ", from,
    paste0("WHERE iso3 = '",countrycode,"'")
  )
}


# extract tl3 - hpi level crosswalk for each country------------------------------------
extract_hpi_ids <- function(isoid,hpilevel) {
  quoisoid <- enquo(isoid)
  quohpilevel <- enquo(hpilevel)
  hpi_id_pos <- ifelse({{hpilevel}}=="tl1",3,2) # position of hpi-level id in extracted dataframe


  read_sf(
    con,
    query = tl_query(select = paste0("tl3_id, ",hpilevel,"_id,iso3"),from = "tl3",countrycode = isoid)) %>%
    as.data.frame() %>%
    select(tl3_id,hpi_id=hpi_id_pos)
}


# connect ------------------------------------------------------------------------------
con <- DBI::dbConnect(
  Postgres(),
  dbname = "oecd_tl",
  host = "gis.main.oecd.org",
  port = 5432,
  user = str_to_title(Sys.getenv("USERNAME"))
)

hpi_regions_rest <- map2_dfr(country_hpi_level$country,country_hpi_level$hpi_level,~bind_rows(extract_hpi_ids(isoid=.x,hpilevel=.y))) %>%
  filter(!grepl("US",tl3_id,fixed=T))

DBI::dbDisconnect(con)

# import function to compute chained laspyeres -----------------------------------------
source(file.path(path_code))

# import data: GHD, HPI, demographic  --------------------------------------------------
ghd_input <- map_dfr(countrylist, ~bind_rows(rio::import(paste0(path_ghd_data,"\\",.x,"_data.rds")))) %>%
  distinct() %>%
  filter(iso3!="USA") %>%
  filter(iso3!="ESP" | geo_type!="District") %>%
  inner_join(hpi_regions_rest, by=c("tl3_id")) %>%
  mutate(datevar=paste0(year,".",quarter),
         hpi_id=tolower(hpi_id),
         price_best=case_when(iso3 %in% c("USA","GBR","IRL") ~ price_av,
                              iso3 %in% c("ESP","DEU","FIN","DNK","PRT","MEX","NOR","BEL","EST","HUN","KOR","SWE","ISR") ~ price_av_m2,
                              iso3 %in% c("FRA","AUT") ~ price_md_m2),
         datevar=case_when(iso3=="FIN" & datevar=="2021.NA" ~ "2021.2",
                           TRUE ~ datevar),
         quarter=case_when(iso3=="FIN"&datevar=="2021.2" ~ 2,
                           TRUE ~ quarter)) %>%
  select(iso3,hpi_id,tl3_id,tl3_name,fua_id,fua_name,is_core,geo_id,geo_name,geo_type,datevar,year,quarter,nb_tra,price_best,best_price,price_av,price_av_m2,price_md,price_md_m2,price_adj,price_adj_m2,size_av,size_md,size_adj,L)

geocol <- ghd_input %>% distinct(iso3,geo_id)

timecol <- ghd_input %>% expand(nesting(iso3,year,quarter)) %>% mutate(datevar=paste0(year,".",quarter)) %>% select(-year,-quarter)

geotime <- full_join(timecol,geocol, by="iso3")

rm(geocol,timecol)

ghd <- geotime %>% left_join(ghd_input, by=c("iso3","geo_id","datevar")) %>%
  group_by(iso3,geo_id) %>%
  arrange(datevar) %>%
  fill(hpi_id,tl3_id,tl3_name,fua_id,fua_name,geo_type,geo_name, .direction="down") %>%
  fill(hpi_id,tl3_id,tl3_name,fua_id,fua_name,geo_type,geo_name, .direction="up") %>%
  ungroup() %>%
  mutate(year=str_sub(datevar,1,4),
         quarter=str_sub(datevar,6,6))

rm(geotime)

hpi <- rio::import(file.path(path_hpi_data)) %>% mutate(target=coalesce(target_regional,target_national),
                                                                                                     target=coalesce(target,target_city)) %>%
  filter(frequency=="quarterly" & evolution=="nominal" & target==T & level!="Cities" &
           NAME != "Scotland (GBR)") %>%
  mutate(NAME = case_when(CODE == "dk011" ~ "Byen København (DNK)",
                          CODE == "dk012" ~ "Københavns omegn (DNK)",
                          CODE == "dk013" ~ "Nordsjælland (DNK)",
                          CODE == "dk021" ~ "Østsjælland (DNK)",
                          CODE == "dk022" ~ "Vest- og Sydsjælland (DNK)",
                          CODE == "dk042" ~ "Østjylland (DNK)",
                          CODE == "es41" ~ "Castile and León (ESP)",
                          CODE == "fi1c3" ~ "Päijät-Häme (FIN)",
                          CODE == "no012" ~ "Akershus excluding Bærum (NOR)",
                          CODE == "no053" ~ "Møre og Romsdal and Vestland excluding Bergen (NOR)",
                          TRUE ~ NAME)) %>%
  mutate(level = case_when(level=="Country" ~ "tl1",
                           level=="Large regions" ~ "tl2",
                           level=="Small regions" ~ "tl3",
                           TRUE ~ level)) %>%
  rename(hpi=index) %>%
  select(country,country_name,level,CODE,NAME,datevar,hpi) %>%
  arrange(country) %>%
  inner_join(country_hpi_level %>% select(-price_best), by=c("country","level"="hpi_level"))

map_geo_id_hpi_id <- ghd %>% select(iso3,geo_id,hpi_id) %>% distinct()

demo <- map_dfr(countrylist, ~ bind_rows(rio::import(paste0(path_ghd_data,"\\",.x,"_vars.rds")) %>% mutate(geo_area = as.numeric(geo_area)))) %>%
  inner_join(map_geo_id_hpi_id, by=c("iso3","geo_id")) %>%
  select(iso3,hpi_id,geo_id,pop,geo_area) %>%
  group_by(iso3,hpi_id) %>%
  summarise(across(c(geo_area,pop), ~ sum(., na.rm=TRUE))) %>%
  ungroup() %>%
  mutate(density=pop/geo_area) %>%
  rename(Population = pop,
         `Population density` = density,
         Size = geo_area) %>%
  mutate(across(c(Population,`Population density`,Size), ~ log(., base=exp(1)), .names="log {.col}"))

# compute chained laspeyres index on GHD data ------------------------------------------
ghd_regional <- ghd %>%
  laspeyres("regional") %>%
  select(iso3,hpi_id,datevar,index_chained)


# merge GHD, HPI, and demographic tables -----------------------------------------------
ghd_hpi_demo_regional <- ghd_regional %>%
  inner_join(hpi, by=c("iso3"="country","hpi_id"="CODE","datevar")) %>%
  filter(iso3!="NOR") %>%                                                       #only two TL3 regions and only three TL2 regions, a plot would be useless
  select(iso3,country_name,level,hpi_id,NAME,datevar,index_chained,hpi) %>%
  group_by(iso3,country_name,level,hpi_id,NAME) %>%
  arrange(datevar) %>%
  mutate(across(c(index_chained,hpi), ~ (./lag(., n=4))-1, .names="{.col}_4")) %>%
  ungroup() %>%
  rename(GHD = index_chained_4,
         HPI = hpi_4) %>%
  select(-index_chained,-hpi) %>%
  filter(!is.na(GHD) & !is.na(HPI)) %>%
  mutate(`GHD-HPI Discrepancy`=abs(GHD-HPI)*100) %>%
  select(-GHD,-HPI) %>%
  group_by(iso3,country_name,level,hpi_id,NAME) %>%
  summarise(`Avg GHD-HPI Discrepancy`=mean(`GHD-HPI Discrepancy`, na.rm=TRUE)) %>%
  inner_join(demo, by=c("iso3","hpi_id")) %>%
  pivot_longer(cols=c("Population","log Population","Population density","log Population density","Size","log Size"), names_to="regressor", values_to="regressor value")

ghd_hpi_demo_regional_plots <- ghd_hpi_demo_regional %>%
  filter(level != "tl1") %>%
  group_by(country_name,regressor) %>% nest() %>%
  mutate(plot=pmap(list(data,country_name,regressor), ~ ggplot(data=.x, aes(x=`regressor value`, y=`Avg GHD-HPI Discrepancy`)) +
                geom_point() + geom_smooth(method="lm") +
                labs(title=.y, x=regressor)
  )) %>%
  arrange(regressor,country_name)

dir.create(path_output)
ggsave(
  filename = file.path(path_output,"discrepancy_vs_demographics_regional.pdf"),
  plot = marrangeGrob(ghd_hpi_demo_regional_plots$plot, ncol=2, nrow=2),
  width = 15, height = 9
)