packages <- c("gridExtra","rio","sf","tidyverse","RPostgres")
# if the packages above are not installed, please uncomment and run the line below to install them before loading them
# invisible(lapply(packages, function(x {install.packages(x)})))
invisible(lapply(packages, function(x) {library(x, quietly=T, character.only=T)}))


# This script only runs if s2 is disabled. The newer versions of {sf} enable s2
# by default. s2 makes area/intersection calculations more accurate.
# Unfortunately, the shapefiles we are using have correct planar, but not
# spherical, geometries. With s2 enabled, we will get the following error:
#
# Evaluation error: Found 1 feature with invalid spherical geometry.
if (packageVersion("sf") >= "1.0") {
  sf::sf_use_s2(FALSE)
}


# Main paths
path_uk_sfpc <- r"(\\main.oecd.org\ASgenECO\HOUSING\DATA\National transaction data\UK - Shapefiles\UK_Postcodes\PostalSector.shp)"
path_ghd_data <- r"(\\main.oecd.org\ASgenECO\HOUSING\DATA\National transaction data\_DATA)"
main_path <- file.path("S:","Data","Prices","Housing","20.Horizontal_project_ECO_CFE_SDD","20.3.Data","HPI","GBR_in_depth_analysis")
path_laspeyres <- file.path(main_path,"C.Programming","laspeyres_index_national_regional.R")
path_hpi_data <- file.path(main_path,"A.Original_Data")
path_output <- file.path(main_path,"B.Output","Local_Authority_level")


# Import shapefiles at postcode (for GHD) and Local Authority (SAU3 file) (for HPI) level

postalsector <- st_read(path_uk_sfpc) %>% select(StrSect) %>% st_transform(4326)
st_crs(postalsector) <- 4326
postalsector <- postalsector %>% mutate(geo_area = st_area(geometry))
# light_postalsector <- st_drop_geometry(postalsector)

con <- DBI::dbConnect(
  Postgres(),
  dbname = "oecd_tl",
  host = "gis.main.oecd.org",
  port = 5432,
  user = str_to_title(Sys.getenv("USERNAME"))
)

SAU3 = st_read(con, query = "SELECT * FROM sau.gbr_sau3_2020") %>%
  mutate(geo_type="SAU3") %>% select(laucode,name_en) %>% st_transform(4326)
st_crs(SAU3) <- 4326
# light_SAU3 <- st_drop_geometry(SAU3)

DBI::dbDisconnect(con)

sf::sf_use_s2(FALSE)


# Intersect postcodes with Local Authorities
mapping_fun <- function(postalsector,SAU3) {
  sf::sf_use_s2(FALSE)
  intersect <- as.data.frame(st_intersection(SAU3,postalsector)) %>%
    inner_join(postalsector %>%
                 mutate(geo_id_area=st_area(geometry)) %>%
                 as.data.frame() %>% select(-geometry),
               by=c("StrSect")) %>%
    mutate(int_area = st_area(geom),
           share=as.numeric(int_area/geo_id_area)) %>%
    as.data.frame() %>% select(-geom) %>%
    group_by(StrSect) %>%
    filter(share>0.5) %>%
    select(-int_area,-geo_id_area,-share)
}

map_geo_id_LA <- mapping_fun(postalsector, SAU3)

rm(con,postalsector,SAU3,light_postalsector,light_SAU3)


# Import Laspeyres function and GHD data and compute chained Laspeyres index
source(path_laspeyres)

ghd <- rio::import(file.path(path_ghd_data,"gbr_data.rds")) %>%
  mutate(datevar = paste0(year,".",quarter)) %>%
  filter(!is.na(price_av) & !is.na(nb_tra)) %>%
  inner_join(map_geo_id_LA %>% select(-name_en), by=c("geo_id"="StrSect")) %>% #278 postcodes in the GHD are not in the lookup table
  mutate(geo_name="") %>%
  rename(hpi_id = laucode,
         price_best = price_av) %>%
  select(-geo_type,-price_md,-price_adj,-size_av,-size_md,-size_adj,-price_av_m2,-price_md_m2,-price_adj_m2) %>%
  laspeyres("regional")

nb_tra_LA <- rio::import(file.path(path_ghd_data,"gbr_data.rds")) %>%
  mutate(datevar = paste0(year,".",quarter)) %>%
  filter(!is.na(price_av) & !is.na(nb_tra)) %>%
  inner_join(map_geo_id_LA %>% select(-name_en), by=c("geo_id"="StrSect")) %>% #278 postcodes in the GHD are not in the lookup table
  mutate(geo_name="") %>%
  rename(hpi_id = laucode,
         price_best = price_av) %>%
  select(hpi_id,geo_id,datevar,nb_tra) %>% 
  group_by(hpi_id,datevar) %>% 
  summarise(nb_tra=sum(nb_tra, na.rm=T)) %>% 
  ungroup()

# Import HPI data
hpi <- read.csv(file.path(path_hpi_data,"UK-HPI-full-file-2021-12.csv")) %>%
  mutate(quarter = case_when(str_sub(Date,4,5) %in% c("01","02","03") ~ "1",
                             str_sub(Date,4,5) %in% c("04","05","06") ~ "2",
                             str_sub(Date,4,5) %in% c("07","08","09") ~ "3",
                             str_sub(Date,4,5) %in% c("10","11","12") ~ "4"),
         year = str_sub(Date,-4),
         datevar = paste0(year,".",quarter)) %>%
  filter(datevar >= 2017) %>%
  select(Date,datevar,year,quarter,RegionName,AreaCode,Index,SalesVolume) %>%
  group_by(datevar,RegionName,AreaCode) %>%
  summarise(hpi = mean(Index),
            SalesVolume = sum(SalesVolume, na.rm=T)) %>%
  ungroup()


# Join GHD and HPI data
ghd_hpi <- ghd %>%
  inner_join(nb_tra_LA, by=c("hpi_id","datevar")) %>% 
  inner_join(hpi, by=c("hpi_id"="AreaCode","datevar")) %>% # 8 Local Authorities are dropped: Daventry, "South Northamptonshire", Northampton, Wellingborough, "East Northamptonshire", Kettering, Corby
  group_by(iso3,hpi_id,RegionName) %>%
  arrange(datevar) %>%
  mutate(across(c(index_chained,hpi), ~ (./dplyr::lag(., n=4))-1, .names="{.col}_4")) %>%
  ungroup() %>%
  rename(GHD = index_chained_4,
         HPI = hpi_4) %>%
  select(-index_chained,-hpi,-year,-quarter) %>% 
  filter(datevar>=2018) %>% 
  relocate(nb_tra,SalesVolume, .after=HPI)


# plots
ghd_hpi_plots <- ghd_hpi %>% 
  pivot_longer(cols=c("GHD","HPI"), names_to="variable", values_to="value") %>% 
  group_by(RegionName) %>% nest() %>% 
  mutate(plot = map2(data,RegionName, ~ ggplot(data=.x, aes(x=datevar, y=value, group=variable, color=variable)) +
                       geom_line(size=1.3) + labs(title=.y) +
                       #scale_y_continuous(labels = label_number(accuracy=0.01)) +
                       theme(axis.text.x = element_text(angle = 45, vjust = 0.5, hjust=1),
                             axis.title.x = element_blank(),
                             legend.title = element_blank(),
                             legend.text = element_text(size=c(12,12)),
                             plot.margin = unit(c(0,0.5,1,0.5), "cm"))))

ggsave(
  filename=file.path(path_output,"YonY_growth_rates_QbyQ_GHD_HPI_LocalAuthority.pdf"),
  plot=marrangeGrob(ghd_hpi_plots$plot, nrow=2, ncol=2),
  width=15, height=9
)

nb_tra_plots <- ghd_hpi %>%
  rename(`num.\ntra.\nGHD`=nb_tra,
         `num.\ntra.\nHPI`=SalesVolume) %>% 
  pivot_longer(cols=c(`num.\ntra.\nGHD`,`num.\ntra.\nHPI`), names_to="variable", values_to="value") %>% 
  group_by(RegionName) %>% nest() %>% 
  mutate(plot = map2(data,RegionName, ~ ggplot(data=.x, aes(x=datevar, y=value, group=variable, color=variable)) +
                       geom_line(size=1.3) + labs(title=.y) +
                       #scale_y_continuous(labels = label_number(accuracy=0.01)) +
                       theme(axis.text.x = element_text(angle = 45, vjust = 0.5, hjust=1),
                             axis.title.x = element_blank(),
                             legend.title = element_blank(),
                             legend.text = element_text(size=c(12,12)),
                             plot.margin = unit(c(0,0.5,1,0.5), "cm"))))

ghd_hpi_nb_tra_plots <- bind_rows(ghd_hpi_plots,nb_tra_plots) %>% 
  arrange(RegionName)

ggsave(
  filename=file.path(path_output,"YonY_growth_rates_QbyQ_GHD_HPI_nb_tra_LocalAuthority.pdf"),
  plot=marrangeGrob(ghd_hpi_nb_tra_plots$plot, nrow=2, ncol=2),
  width=15, height=9
)


# excel tables
ghd_hpi_tables <- ghd_hpi %>% 
  filter(datevar>=2019.1 & datevar<=2021.1) %>% 
  mutate(discrepancy=abs(GHD-HPI),
         periods=ifelse(datevar<=2020.1,"period 1","period 2")) %>% 
  group_by(RegionName) %>% 
  mutate(avg_discrepancy=mean(discrepancy, na.rm=TRUE),
         avg_discrepancy_1=mean(discrepancy[periods=="period 1"], na.rm=TRUE),
         avg_discrepancy_2=mean(discrepancy[periods=="period 2"], na.rm=TRUE),
         avg_HPI = mean(HPI, na.rm=TRUE),
         avg_abs_HPI=mean(abs(HPI), na.rm=TRUE),
         avg_discrepancy_ratio=avg_discrepancy/avg_abs_HPI) %>%
  arrange(avg_discrepancy_ratio) %>%
  ungroup() %>% 
  select(-periods,-avg_abs_HPI,-avg_discrepancy_ratio) %>% 
  pivot_longer(cols=c("GHD","HPI","discrepancy"), names_to="variable", values_to="value") %>%
  pivot_wider(names_from=datevar, values_from=value) %>%
  rename(`Average Discrepancy`=avg_discrepancy,`Average Discrepancy 2019Q1-2020Q1`=avg_discrepancy_1,`Average Discrepancy 2020Q2-2021Q1`=avg_discrepancy_2,`Average Y-on-Y growth rate of HPI`=avg_HPI)

rio::export(ghd_hpi_tables, file.path(path_output,"ghd_hpi_discrepancy_LA.xlsx"))

# import demographic data (postcode-level population, area) and export plots
demo <- rio::import(file.path(path_ghd_data,"gbr_vars.rds")) 

discrepancy_demo_plots <- demo %>% 
  inner_join(map_geo_id_LA, by=c("geo_id"="StrSect")) %>%
  inner_join(ghd_hpi %>% ungroup(), by=c("laucode"="hpi_id")) %>%
  mutate(discrepancy=abs(GHD-HPI)) %>%
  group_by(RegionName,laucode,datevar) %>% 
  summarise(`GHD-HPI Discrepancy`=mean(discrepancy,na.rm=T),
            Population=sum(pop,na.rm=T),
            Size=sum(geo_area)) %>%
  ungroup() %>% 
  group_by(RegionName,laucode) %>% 
  summarise(`Avg GHD-HPI Discrepancy`= mean(`GHD-HPI Discrepancy`, na.rm=TRUE ),
            Population=mean(Population, na.rm=TRUE),
            Size=mean(Size, na.rm=TRUE)) %>% 
  ungroup() %>% 
  mutate(`Population Density`=Population/Size,
         `log Size`=log(Size),
         `log Population`=log(Population),
         `log Population Density`=log(`Population Density`)) %>% 
  pivot_longer(cols=c("Population","Size","Population Density","log Population","log Size","log Population Density"), names_to="regressor", values_to="regressor value") %>% 
  group_by(regressor) %>% nest() %>% 
  mutate(plot=map2(data,regressor, ~ ggplot(data=.x, aes(x=`regressor value`, y=`Avg GHD-HPI Discrepancy`)) + 
                     geom_point() + geom_smooth(method="lm") +
                     labs(title=.y, x=regressor)
                   )) %>%
  ungroup() %>% 
  arrange(regressor)

ggsave(
  filename = file.path(path_output,"discrepancy_demographics_LocalAuthority.pdf"),
  plot = marrangeGrob(discrepancy_demo_plots$plot, ncol=2, nrow=2),
  width = 15, height = 9
)

